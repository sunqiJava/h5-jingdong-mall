var mySwiper = new Swiper ('#one', {

    direction: 'horizontal',
    loop: true,
    effect: 'fade',
    // 如果需要分页器
    pagination: '.swiper-pagination',
    paginationClickable :true,
    // 如果需要前进后退按钮
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    autoplay:true,//开启轮播图
})

var Swiper3 = new Swiper('#there',{
    direction: 'horizontal',
    loop: true,
    // 如果需要前进后退按钮
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',

})

var Swiper2 = new Swiper('.swiper2',{
    direction: 'horizontal',
    loop: true,


    // 如果需要前进后退按钮
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',

})

var four = new Swiper('#four', {
    direction: 'horizontal',
    loop: true,
    slidesPerView: 5,
    autoplay:true


})

window.onload=function() {
    /* 秒杀倒计时 */
    var hour = document.querySelector(".hour");//小时的黑色盒子
    var minute = document.querySelector(".minute");//分钟的黑色盒子
    var second = document.querySelector(".second");//秒数的黑色盒子
    var inputTime = +new Date('2022-11-12 21:00:00');//倒计时的结束时间，自己设置时间,返回的是用户输入时间的总毫秒数
    //此处时间一定要比当前时间大，不然不会倒计时
    countDown();
    //2、开启定时器
    setInterval(countDown,1000);
    function countDown() {
        var nowTime = +new Date();//返回的是当前时间的总毫秒数
        //var times = (inputTime-nowTime) / 1000;//times是剩余时间的总毫秒数
        //上边写法在倒计时归零后还会继续以“0-xxx“的形式继续减少，所以改成下边写法
        var times = inputTime>nowTime?(inputTime - nowTime) / 1000:0;  //到时间就不会继续减了

        var h = parseInt(times / 60 / 60);//时
        h = h < 10 ? "0" + h : h;
        hour.innerHTML = h;//把剩余的小时给小时黑色盒子
        var m = parseInt(times / 60 % 60);//分
        m = m < 10 ? "0" + m : m;
        minute.innerHTML = m;
        var s = parseInt(times % 60);//秒
        s = s < 10 ? "0" + s : s;
        second.innerHTML = s;

    }
}
//楼层效果
/*右侧边栏效果*/
let elevator = document.querySelector('.elevator_list');
let logo_1 = document.querySelector('.logo_1');

//给页面绑定滚动监听事件
window.onscroll = function (){
    //获取滚动条距上面的距离
    let top = document.documentElement.scrollTop || document.body.scrollTop;
    //获取元素距上面的距离
    let top1 = 745;
    if(top >= top1){
        elevator.className = 'elevator_list elevator_list-fix'
    }else {
        elevator.className = 'elevator_list'
    }

    if(top >= top1){
        logo_1.style.position="fixed";
        logo_1.style.display="block";
        logo_1.style.top="0px";

    }else {
        logo_1.style.display="none";
    }
}
/*实现广告效果*/
var guanggao=document.querySelector('.guanggao');
var cha=document.querySelector('.head-2');
cha.onclick=function(){
    this.style.display='none';
    guanggao.style.display='none';
}

let hotWord = document.querySelector('.hot-word');
let hotWords = ['苹果手机','家用电器','电脑','女鞋','户外','显示器','图书' ,'教育', '电子书' ];
let index = 0;
setInterval(function () {
    index++;
    if(index > hotWords.length - 1){
        index = 0;
    }
    hotWord.placeholder = hotWords[index];
}, 3000);




